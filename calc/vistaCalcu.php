<!DOCTYPE html>
<html>
<head>
    <title>Calculadora</title>
</head>
<body>
    <h1>Calculadora</h1>
    <form method="post" action="index.php?method=resultado">
        <input type="text" value="<?php echo isset($operador1) ? $operador1 : '' ?>" name="operador1" onfocus="this.value=''"  size="10">

        <select name = "operacion">
            <option value="suma" <?php if (isset($operacion) && $operacion == "suma") echo 'selected="selected" '; ?>
                >+</option>
            <option value="resta" <?php if (isset($operacion) && $operacion == "resta")  echo 'selected="selected" '; ?>
                >-</option>
            <option value="multiplicación" <?php if (isset($operacion) && $operacion == "multiplicación")  echo 'selected="selected" '; ?>
                >*</option>
            <option value="división" <?php if (isset($operacion) && $operacion == "división")  echo 'selected="selected" '; ?>
                >/</option>
            <option value="potencia" <?php if (isset($operacion) && $operacion == "potencia")  echo 'selected="selected" '; ?>
                >^</option>
        </select>

        <input type="text" value="<?php echo isset($operador2) ? $operador2 : '' ?>" name="operador2" onfocus="this.value=''"  size="10">

        <input type="submit" value="=" name="igual"  size="10">

        <input type="text"  value="<?php echo isset($resultado) ? $resultado : '' ?>" name="total" readonly  size="30">
    </form>
    <hr>
</body>
</html>
