<!DOCTYPE html>
<html>
<head>
    <title>Calculadora</title>
</head>
<body>
    <h1>Calculadora</h1>
    <form method="post" action="App.php">
        <input type="text" value="<?php echo $operador1?>" name="operador1" onfocus="this.value=''" size="10">
        <select name = "operación">
            <option value="suma" <?php if ($_POST['operación'] == "suma") echo 'selected="selected" '; ?> >+</option>
            <option value="resta" <?php if ($_POST['operación'] == "resta") echo 'selected="selected" '; ?> >-</option>
            <option value="multiplicación" <?php if ($_POST['operación'] == "multiplicación") echo 'selected="selected" '; ?> >*</option>
            <option value="división" <?php if ($_POST['operación'] == "división") echo 'selected="selected" '; ?> >/</option>
            <option value="potencia" <?php if ($_POST['operación'] == "potencia") echo 'selected="selected" '; ?> >^</option>
        </select>
        <input type="text" value="<?php echo $operador2?>" name="operador2" onfocus="this.value=''"  size="10">
        <input type="submit" value="=" name="igual"  size="10">
        <input type="text" name="total" value="<?php echo $resultado ?>" readonly  size="30">
    </form>
    <hr>
</body>
</html>
