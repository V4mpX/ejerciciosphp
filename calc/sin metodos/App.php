<?php
   $operación = isset($_REQUEST['operación']) ? $_REQUEST['operación'] : false;
   $operador1 = isset($_REQUEST['operador1']) ? $_REQUEST['operador1'] : false;
   $operador2 = isset($_REQUEST['operador2']) ? $_REQUEST['operador2'] : false;

   if (ctype_digit($operador1)) {
      if (ctype_digit($operador2)) {
         switch ($operación) {
            case "suma":
               $resultado = $operador1 + $operador2;
               break;
            case "resta":
               $resultado = $operador1 - $operador2;
               break;
            case "multiplicación":
               $resultado = $operador1 * $operador2;
               break;
            case "división":
               $resultado = $operador1 / $operador2;
               break;
            case "potencia":
               $resultado = $operador1 ** $operador2;
               break;
         }
      }else{
         $resultado = "No has introducido un numero";
      }
   }else{
         $resultado = "No has introducido un numero";
   }


   include "vistaCalcu.php";
