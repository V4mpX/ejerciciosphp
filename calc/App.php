<?php
 class App
 {

   function __construct(){
      session_start();
   }

   public function operacion(){
        //este metodo muestra la vista de la calculadora
        //$operacion = ''; ya comprobamos si existe la operacion en vistaCalcu
        require('vistaCalcu.php');
   }

   public function resultado(){
    // comprobamos si los datos están introducido
        if(isset($_REQUEST['operador1']) && !empty($_REQUEST['operador1'] && $_REQUEST['operador2']) && !empty($_REQUEST['operador2'])   ){
            //creamos una sesion para recordar la operación anterior
            $operador1 = $_REQUEST['operador1'];
            $_SESSION['operador1'] = $operador1;

            $operacion = $_REQUEST['operacion'];
            $_SESSION['operacion'] = $operacion;

            $operador2 = $_REQUEST['operador2'];
            $_SESSION['operador2'] = $operador2;

            //comprobamos si son numeros válidos
            if(!ctype_digit($operador1)){
                $resultado = 'Operador 1 no valido';
                require('vistaCalcu.php');
                return;
            }

            if(!ctype_digit($operador2)){
                $resultado = 'Operador 2 no valido';
                require('vistaCalcu.php');
                return;
            }
            //switch case para cada operación
            switch ($operacion) {
                case "suma":
                    $resultado = $operador1 + $operador2;
                    break;
                case "resta":
                    $resultado = $operador1 - $operador2;
                    break;
                case "multiplicación":
                    $resultado = $operador1 * $operador2;
                    break;
                case "división":
                    $resultado = $operador1 / $operador2;
                    break;
                case "potencia":
                    $resultado = $operador1 ** $operador2;
                    break;
            }
        }else{ //y si no hay algun numero introducido aparecerá este mensaje
            $resultado = 'Faltan datos';
        }
        require('vistaCalcu.php');
    }
}
