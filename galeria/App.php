<?php
 class App
 {

    public function home(){
        require('home.php');
    }

    public function upload(){

        $target_dir = "uploads/";
        $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
        $uploadOk = 1;
        $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
        // Comprobamos si el archivo de imagen es una imagen falsa o verdadera
        if(isset($_POST["submit"])) {
            $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
            if($check !== false) {
                //echo "El archivo es una imagen";
                $uploadOk = 1;
            } else {
                //echo "El archivo no es una imagen.";
                $uploadOk = 0;
            }
        }
        // Comprueba si el archivo ya existe en el servidor
        if (file_exists($target_file)) {
            //echo "El archivo existe.";
            $uploadOk = 0;
        }
        // Comprueba si el archivo es grande
        if ($_FILES["fileToUpload"]["size"] > 500000) {
            //echo "Ocupa demasiado el archivo";
            $uploadOk = 0;
        }
        // Solo deja unos tipos de archivo
        if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
        && $imageFileType != "gif" ) {
            //echo "Solo archivos JPG, JPEG, PNG & GIF son permitidos";
            $uploadOk = 0;
        }
        // Comprueba la variable $uploadOk es 0 por algun error
        if ($uploadOk == 0) {
            $mensaje = "No se ha subido el archivo";
        // si está todo bien, subirá el archivo
        } else {
            if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
                $mensaje = "El archivo ". basename( $_FILES["fileToUpload"]["name"]). " fue subido.";
            } else {
                $mensaje = "No se ha subido el archivo";
            }
        }
        require("subirFotos.php");
    }

    public function imagenes(){

        $path = "uploads/";
        $directorio = opendir($path); //ruta actual
        while ($archivo = readdir($directorio)) {
            //verificamos si es o no un directorio
            if (!is_dir($archivo)) {
                $fotos[] = $path . $archivo;
            }
        }


        require("verFotos.php");
    }

    public function detalle(){
        $foto=$_REQUEST['file'];
        require("detalleFotos.php");
    }

    public function borrar(){
        //borra el archivo
        $foto = $_REQUEST['file'];
        unlink($foto);
        require ("detalleFotos.php");
    }

}
