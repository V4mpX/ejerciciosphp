<!DOCTYPE html>
<html>
    <head>
        <title>ejercicio2</title>
    </head>
    <body>
        <?php
            $precioUd = 21;
            $cantidad = 3;
            $iva = 1.21;
            $precioSin = $precioUd * $cantidad;
            $precioCon = $precioUd * $cantidad * $iva;

            echo "<p> Precio unidad: $precioUd</p>";
            echo "<p> Cantidad: $cantidad</p>";
            echo "<p> IVA: $iva</p>";
            echo "<p> Precio sin IVA: $precioSin</p>";
            echo "<p> Precio con IVA: $precioCon</p>";
        ?>
    </body>
</html>
