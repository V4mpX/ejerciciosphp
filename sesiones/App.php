<?php

/**
*
*/
class App
{

    function __construct()
    {
        // echo "App!!<br>";
        session_start();
    }

    public function login()
    {
        // echo "login!!<br>";
        require('viewLogin.php');
    }

    public function auth()
    {
        echo "Auth!!<br>";
        //tomar usuario y meter en sesion
        if (isset($_REQUEST['user']) && !empty($_REQUEST['user'])) {
            $user = $_REQUEST['user'];
            $_SESSION['user'] = $user;
            header('Location:index.php?method=home');
        } else {
            //reenviar a home
            header('Location:index.php?method=login');
        }
        return;
    }

    public function home()
    {
        if (!isset($_SESSION['user'])) {
            header('Location:index.php?method=login');
            return;
        }

        $user = $_SESSION['user'];
        if (isset($_SESSION['deseos'])) {
            $deseos = $_SESSION['deseos'];
        } else {
            $deseos = [];
        }
        require('viewList.php');
    }

    public function new()
    {
        if (isset($_REQUEST['deseo']) && !empty($_REQUEST['deseo'])) {
            $_SESSION['deseos'][] = $_REQUEST['deseo'];

            // $deseos =  $_SESSION['deseos'];
            // $deseos[] = $_REQUEST['deseo'];
            // $_SESSION['deseos'] = $deseos;
        }
        header('Location:?method=home');
    }

    public function close()
    {
        echo "Close!!";
        session_destroy();
        unset($_SESSION);
        header('Location:index.php?method=login');
    }

    public function delete()
    {
        echo "Delete $_REQUEST[key]!!";
        $key = (integer) $_REQUEST['key'];
        unset($_SESSION['deseos'][$key]);
        header('Location:?method=home');
    }

    public function empty()
    {
        unset($_SESSION['deseos']);
        header('Location:?method=home');
    }
}
