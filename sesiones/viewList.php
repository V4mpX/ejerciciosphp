<!DOCTYPE html>
<html>
<head>
    <title>Sesiones</title>
</head>
<body>
<h1>Lista de deseos (sesiones) de
    <?php echo isset($_SESSION['user']) ? $_SESSION['user'] : ''  ?>
</h1>
    <p><a href="?method=close">Cerrar sesión</a></p>

    <h3>Nuevo</h3>
    <form method="post" action="?method=new">
        <label>Nuevo deseo</label><input type="text" name="deseo">

        <input type="submit" name="">
    </form>
    <hr>
    <h3>Lista de deseos</h3>

    <ul>
        <?php foreach ($deseos as $clave => $deseo): ?>
            <li><?php echo $deseo ?>
                <a href="?method=delete&key=<?php echo $clave ?>">Borrar</a>
            </li>
        <?php endforeach ?>
    </ul>

    <p><a href="?method=empty">Vaciar lista</a></p>
</body>
</html>
