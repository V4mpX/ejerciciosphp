<?php

/**
* Clase principal de mi aplicación
*/
class App
{
    private $atributo;

    function __construct($param)
    {
        // echo "Construyendo mi App<br>";
        $this->atributo = $param;
    }

    public function index()
    {
        if (isset($_COOKIE['nombre'])) {
            $name = $_COOKIE['nombre'];
        }
        require('viewLogin.php');
    }
    public function hello()
    {
        echo "Hello world";
    }
    public function home()
    {
        if (isset($_REQUEST['name']) && !empty($_REQUEST['name'])) {
            $name = $_REQUEST['name'];
            if (isset($_REQUEST['remember'])) {
                setcookie('nombre', $name);
            } else {
                setcookie('nombre', $name, 1);
            }
        }
        require('viewHome.php');
    }
}
