<!DOCTYPE html>
<html>
    <head>
        <title>ejercicio3</title>
    </head>
    <body>
        <?php

            $semana = array('Lunes', 'Martes', 'Miercoles', 'Jueves', 'Jueves', 'Sabado');

            $semana[] = 'Domingo';
            $semana[4] = 'Viernes';

            #para ver errores

            #echo "<pre>";
            #var_dump($semana);
            #echo "</pre>";
            #exit();


            #ul
            foreach ($semana as $dia) {
                echo "<ul>$dia</ul>";
            }
        ?>

        <!-- tabla -->
        <table border="1px">
         <?php foreach ($semana as $orden => $dia): ?>
                <tr>
                    <td><?php echo $orden + 1 ?></td>
                    <td><?php echo $dia ?></td>
                </tr>
             <?php endforeach ?>
        </table>
    </body>
</html>
